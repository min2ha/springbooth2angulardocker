package lt.archivo.SpringBootH2AngularDocker.repository;

import lt.archivo.SpringBootH2AngularDocker.model.Results;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource()
public interface ResultsRepository extends JpaRepository<Results, Integer>,
        JpaSpecificationExecutor<Results>,
        QuerydslPredicateExecutor<Results> {}



