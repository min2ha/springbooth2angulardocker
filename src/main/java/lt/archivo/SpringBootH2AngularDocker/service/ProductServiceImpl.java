package lt.archivo.SpringBootH2AngularDocker.service;

import lt.archivo.SpringBootH2AngularDocker.model.CustomerRequest;
import lt.archivo.SpringBootH2AngularDocker.model.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService {
    private static final Logger log = LoggerFactory.getLogger(ProductServiceImpl.class);

    private static Map<String, Product> productRepo = new HashMap<>();
    static {
        String[][] rulesForProducts = {
                //Business Cases, Rules for Business Logic
                //min Income,Max Income,Min Age, Max Age, is Student?, Account title
                { "0",   "40001",     "17","65","false", "Current Account"},  //Current Account      -  Income > 0      &  Age > 17
                {"40000","2147483647","17","65","false","Current Account Plus"},
                {"-1",   "2147483647","0","18","false","Junior Saver Account"},
                {"-1",   "2147483647","17","65","true","Student Account"},
                {"-1",   "2147483647","64","1000","false","Senior Account"},
                {"0",    "12001",     "17","65","false","Debit Card"},
                {"12000","40001",     "18","65","false","Credit Card"},
                {"40000","2147483647","18","65","false","Gold Credit Card"}
        };
        AtomicInteger rows = new AtomicInteger();
        Arrays.stream(rulesForProducts).
                forEach(subArr -> {
                    int row = rows.getAndIncrement();
                    AtomicInteger columns = new AtomicInteger();
                    Arrays.asList(subArr);
                    productRepo.put(String.valueOf(row), new Product( Integer.valueOf(subArr[0]), Integer.valueOf(subArr[1]), Integer.valueOf(subArr[2]), Integer.valueOf(subArr[3]), Boolean.valueOf(subArr[4]), subArr[5]));
                    //Arrays.stream(subArr).
                    //        forEach(e -> iterateAction(
                    //                row, columns.getAndIncrement(),
                    //                e, e == null));
                });

    }

    /***
     * Backend main method (Business Logic)
     * @param customerRequest
     * @return
     *
     * (Based on JVM 8 Lambda functions,
     * more recent versions of JVM could suggest more optimal code)
     * */
    @Override
    public List<String > processCustomerRequest(CustomerRequest customerRequest) {
        log.debug("CustomerRequest.ts Data: age = " + customerRequest.getAge() + ", isStudent = " + customerRequest.getStudent() + ", income = "+customerRequest.getIncome());
        //business logic
        return productRepo.values()
                .stream()
                .filter(x -> x.getAgeFrom() < customerRequest.getAge() && x.getAgeUpTo() > customerRequest.getAge())
                .filter(x -> x.getIncomeFrom() < customerRequest.getIncome() && x.getIncomeUpTo() > customerRequest.getIncome())
                .filter(x -> x.getStudent().equals(customerRequest.getStudent()))
                .map(x -> x.getName())
                .collect(Collectors.toList());
    }

    /*
    //For the Result collection, on demand in the future

        @Override
    public void createProduct(Product product) {
        productRepo.put(product.getId(), product);
    }
    @Override
    public void updateProduct(String id, Product product) {
        productRepo.remove(id);
        product.setId(id);
        productRepo.put(id, product);
    }
    @Override
    public void deleteProduct(String id) {
        productRepo.remove(id);
    }
    @Override
    public Collection<Product> getProducts() {
        return productRepo.values();
    }
    *
    * */
}
