package lt.archivo.SpringBootH2AngularDocker.service;

import lt.archivo.SpringBootH2AngularDocker.model.CustomerRequest;
import java.util.List;

public interface ProductService {

    public abstract List<String> processCustomerRequest(CustomerRequest customerRequest);

    /*
    public abstract void createProduct(Product product);
    public abstract void updateProduct(String id, Product product);
    public abstract void deleteProduct(String id);
    public abstract Collection<Product> getProducts();
    * */
}
