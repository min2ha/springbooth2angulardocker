package lt.archivo.SpringBootH2AngularDocker.service;

import lt.archivo.SpringBootH2AngularDocker.model.Results;
import lt.archivo.SpringBootH2AngularDocker.repository.ResultsRepository;
import org.springframework.stereotype.Component;

import java.util.List;

//For the Result collection, on demand in the future
@Component
public class ResultsService {
    private ResultsRepository resultsRepository;
    public ResultsService(ResultsRepository resultsRepository) {
        this.resultsRepository = resultsRepository;
    }
    public List<Results> getResults() {
        return resultsRepository.findAll();
    }
    public Results saveResults(Results users) {
        return resultsRepository.save(users);
    }
}


