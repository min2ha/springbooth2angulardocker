package lt.archivo.SpringBootH2AngularDocker.controller;

import lt.archivo.SpringBootH2AngularDocker.model.CustomerRequest;
import lt.archivo.SpringBootH2AngularDocker.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@CrossOrigin
@RestController
public class ProductServiceController {

    @Autowired
    ProductService productService;

    //@CorsOr(origins="http://localhost:4200")
    @PostMapping(value = "/recommendproductstocustomers")
    public ResponseEntity<Object> recommendListOfProductsToCustomers(@RequestBody CustomerRequest customerRequest) {
        return new ResponseEntity<>(productService.processCustomerRequest(customerRequest), HttpStatus.OK);
    }

    /*
        //For the Result collection, on demand in the future

    @GetMapping(value = "/products")
    public ResponseEntity<Object> getProduct() {
        return new ResponseEntity<>(productService.getProducts(), HttpStatus.OK);
    }
    @PutMapping(value = "/products/{id}")
    public ResponseEntity<Object>
    updateProduct(@PathVariable("id") String id, @RequestBody Product product) {

        productService.updateProduct(id, product);
        return new ResponseEntity<>("Product is updated successsfully", HttpStatus.OK);
    }
    @DeleteMapping(value = "/products/{id}")
    public ResponseEntity<Object> delete(@PathVariable("id") String id) {
        productService.deleteProduct(id);
        return new ResponseEntity<>("Product is deleted successsfully", HttpStatus.OK);
    }
    @PostMapping(value = "/products")
    public ResponseEntity<Object> createProduct(@RequestBody Product product) {
        productService.createProduct(product);
        return new ResponseEntity<>("Product is created successfully", HttpStatus.CREATED);
    }
     */


}