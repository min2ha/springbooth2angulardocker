package lt.archivo.SpringBootH2AngularDocker.model;

public class CustomerRequest {

    //get int values for business logic?

    private Integer age;
    private Boolean student;
    private Integer income;


    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Boolean getStudent() {
        return student;
    }

    public void setStudent(Boolean student) {
        this.student = student;
    }

    public Integer getIncome() {
        return income;
    }

    public void setIncome(Integer income) {
        this.income = income;
    }
}
