package lt.archivo.SpringBootH2AngularDocker.model;

import com.sun.istack.NotNull;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

//For the Result collection, on demand in the future
@Entity
@Data
public class Results {

    @Id
    @Column
    private long id;

    @Column
    //@NotNull(message="{NotNull.Results.age}")
    private String age;

    @Column
    //@NotNull(message="{NotNull.Results.studentStatus}")
    private String studentStatus;

    @Column
    //@NotNull(message="{NotNull.Results.income}")
    private String income;

    @Column
    //@NotNull(message="{NotNull.Results.product}")
    private String product;

}
