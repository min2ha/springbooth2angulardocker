package lt.archivo.SpringBootH2AngularDocker.model;

public class Product {
    private String id;
    private String name;


    private Integer incomeFrom;
    private Integer incomeUpTo;

    private Integer ageFrom;
    private Integer ageUpTo;

    private Boolean student;



    public Product() {
    }

    public Product( Integer incomeFrom, Integer incomeUpTo, Integer ageFrom, Integer ageUpTo, Boolean student, String name) {
        this.name = name;
        this.incomeFrom = incomeFrom;
        this.incomeUpTo = incomeUpTo;
        this.ageFrom = ageFrom;
        this.ageUpTo = ageUpTo;
        this.student = student;
    }




    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIncomeFrom() {
        return incomeFrom;
    }

    public void setIncomeFrom(Integer incomeFrom) {
        this.incomeFrom = incomeFrom;
    }

    public Integer getIncomeUpTo() {
        return incomeUpTo;
    }

    public void setIncomeUpTo(Integer incomeUpTo) {
        this.incomeUpTo = incomeUpTo;
    }

    public Integer getAgeFrom() {
        return ageFrom;
    }

    public void setAgeFrom(Integer ageFrom) {
        this.ageFrom = ageFrom;
    }

    public Integer getAgeUpTo() {
        return ageUpTo;
    }

    public void setAgeUpTo(Integer ageUpTo) {
        this.ageUpTo = ageUpTo;
    }

    public Boolean getStudent() {
        return student;
    }

    public void setStudent(Boolean student) {
        this.student = student;
    }
}
