package lt.archivo.SpringBootH2AngularDocker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootH2AngularDockerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootH2AngularDockerApplication.class, args);
	}

}
