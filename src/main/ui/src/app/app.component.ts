import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {MatSliderChange} from "@angular/material/slider";
import {MatSlideToggleChange} from "@angular/material/slide-toggle";
import {CustomerRequest} from "./customer-request.model";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit{
  title = 'Recommend list of products to perspective customers';

  public model: CustomerRequest;
  private baseUrl = 'http://localhost:8080/recommendproductstocustomers';

  constructor(private httpClient: HttpClient) {
    this.model = new CustomerRequest();
    this.model.age = 0;
    this.model.income = 0;
    this.model.student = false;
  }

  productList:string;

  ngOnInit() {
    const headers = { 'content-type': 'application/json'}
    this.httpClient.post<any>(this.baseUrl, this.model, {'headers':headers}).subscribe(data => {
      this.productList = data;
    })
  }

  onAgeInputChange(event: MatSliderChange) {
    this.model.age = event.value;
  }
  onIncomeInputChange(event: MatSliderChange) {
    this.model.income = event.value * 1000;
  }
  onStudentStatusInputChange(event: MatSlideToggleChange) {
    this.model.student = event.checked;
  }
  //get data from Backend
  productsCheck(){
    const headers = { 'content-type': 'application/json'}
    this.httpClient.post<any>(this.baseUrl, this.model, {'headers':headers}).subscribe(availableProducts => {
      if(availableProducts == "") {
        this.productList = "No products available";
      }
      else{
        this.productList = availableProducts;
      }
    })
  }
  formatAgeLabel2(value: string) {
    return value;
  }
  //0-17, 18-64, 65+
  formatAgeLabel(value: number) {
    if (value>=65)
      return value+'+';
    return value;
  }
  //0, 1-12000, 12001-40000, 40001+
  formatIncomeLabel(value: number) {
    if (value>=50)
      return value+'k+';

    return Math.round(value) + 'k';
  }
}

