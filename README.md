# Banking - Product Tool (demo excersise).

The bank is producing a tool to recommend list of products to perspective customers.
The rules which govern what products a customer may choose are based upon answers to questions that the customer has given.

## Architecture

Backend (port 8080) <---> Frontend (port 4200)

RESTful web service will include CORS access control headers in its response

Where:
* Backend - Spring Boot RESTful web service 
* Frontend (UI) - Angular 


## Installation

### Prequisites

I'm expecting that you've already installed the following prequisites (or any compatible):

* Java JDK (1.8+)
* Git (2.25.x+)
* Maven (3.6.x)
* npm (6.x.x+)
* Node (v14.0.0)
* Anguar CLI (version 9.1.5)


## Steps to run

1. clone repository git clone git@bitbucket.org:min2ha/springbooth2angulardocker.git
2. cd to main directory: `cd springbooth2angulardocker`
3. Build the project using `mvn clean install`
4. Run backend using `mvn spring-boot:run`
5. cd to UI root: `cd src/main/ui` 
6. get dependencies: `npm install` 
7. cd to app directory: `cd src/app` 
8. run frontend using `ng serve`
9. The web application is accessible via localhost:4200


## Backend

Backend - Spring Boot (version 2.3.5)


## Ui

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.5.

### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
